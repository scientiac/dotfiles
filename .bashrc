# .bashrc
# Source global definitions
if [ -f /etc/bashrc ]; then
	source /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
	PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

export PATH="/home/$USER/.local/bin:$PATH"
export PATH="/home/$USER/.emacs.d/bin:$PATH"
export PATH="/home/$USER/Experiments/ongoing/go/bin:$PATH"

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# include config file
if [ -f /home/scientiac/.config/bash/config.sh ]; then
	source /home/scientiac/.config/bash/config.sh
fi

alias luamake=/home/scientiac/Applications/lua-language-server/3rd/luamake/luamake
. "$HOME/.cargo/env"

[ -s ~/.luaver/luaver ] && . ~/.luaver/luaver
[ -s ~/.luaver/completions/luaver.bash ] && . ~/.luaver/completions/luaver.bash

export GOPATH="$HOME/Experiments/ongoing/go/"

[ -f "/home/scientiac/.ghcup/env" ] && source "/home/scientiac/.ghcup/env" # ghcup-env
export PATH=$PATH:/home/scientiac/.spicetify
