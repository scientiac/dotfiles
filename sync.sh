#! /usr/bin/bash

# Assign variables to file paths saperated by space.
paths="/home/scientiac/.config/alacritty/ /home/scientiac/.bashrc /home/scientiac/.bash_profile /home/scientiac/.config/bash/ /home/scientiac/.doom.d/ /home/scientiac/.config/nushell/ /home/scientiac/.config/wezterm/ /home/scientiac/.config/zathura/ /home/scientiac/.config/qutebrowser/ /home/scientiac/.config/nvim/"

# Get filenames from the added paths
files=$(basename -a -z $paths | tr '\0' ' ')

# Bring everything to the dotfiles directory on gitlab.
cp -r $paths /home/scientiac/Experiments/gitsync/dotfiles/

echo "Copied [ "$files"] to /home/scientiac/Experiments/gitsync/dotfiles/"

sleep 1

# Git add all the files that were borught to the dotfiles directory.
git add .
echo "Executed 'git add' on [ "$files"]!"

sleep 1

# Git commit with a message.
git commit -m "Regular Backup: Sync"
echo "Executed 'git commit' with message [Regular Backup: Sync]!"

sleep 1

# Git Push
git push git@gitlab.com:scientiac/dotfiles.git
echo "Executed 'git push'!"

sleep 1
echo "done!"
