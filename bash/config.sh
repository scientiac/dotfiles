# .bashrc
# ============================================================================ #
# ALIASES

eval "$(thefuck --alias)"
eval $(thefuck --alias FUCK) # for thefuck program

# Zoxide
eval "$(zoxide init bash)"
# ============================================================================ #
# SHORTCUTS
alias ga="git add ."
alias gc="git commit -m"
alias gp="git push"
# Add something dumbass

# ============================================================================ #
# Publishing
alias pub="cd /home/scientiac/Things/Linux/tildeverse/workinprogress/publish && ls -1"

#====================aliases===================#
alias ard="cd ~/Arduino/uno/src/ && ls"
alias ardinit="pio project init --board uno"
alias ardup="pio run --target upload"
alias ardsee="pio device monitor"

alias ls="lsd --color auto -1"
# ============================================================================ #
# ============================================================================ #
# STARSHIP

eval "$(starship init bash)"
# ============================================================================ #
# GNOME
alias gnome-shell-restart="xdotool key "alt+F2" && sleep 0.3 && xdotool type "restart" && xdotool key "Return""

# ============================================================================ #
# NIX-ENV
alias nix-set="ln -s /home/$USER/.nix-profile/share/applications/* /home/$USER/.local/share/applications/ ; gnome-shell-restart"

# ============================================================================ #
# SPICETIFY

. "$HOME/.cargo/env"
export PATH=$PATH:/home/scientiac/.spicetify
# ============================================================================ #
# TUI APPS
alias top='bpytop'
alias rm="trash -i"
# ============================================================================ #
# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] &&
	[ -s "$BASE16_SHELL/profile_helper.sh" ] &&
	source "$BASE16_SHELL/profile_helper.sh"

base16_dracula

# ============================================================================ #
alias "ranger"='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias "yt"="/usr/local/bin/ytfzf -t -T chafa"

# ============================================================================ #
alias "lear"="cd /home/scientiac/Programming/normal/rust/learning_rust/practice/get-started/"
alias "ty"="thokr"
alias "tnvim"="/bin/nvim"
# alias "nvim"="neovide --frame none --maximized --multigrid"
# alias "nvim"="goneovim 2>/dev/null"
alias "nivm"="nvim"
# systemctl enable --user emacs.service
# alias "nvim"="em"
# alias "em"='emacs -bg "#21242b" -fs'
# alias "em"="emacsclient -c -F \"'(fullscreen . fullboth)\""
alias "em"="emacsclient -c -F \"((fullscreen . maximized) (undecorated . t))\""

# ============================================================================ #
# RUST SPECIFIC
alias "rrepl"="evcxr"

# ============================================================================ #
# IMAGE RENDERING
# terminal="$(ps -p $PPID -o comm=)"

if [ "$TERM" = "xterm-kitty" ]; then
	kitty +kitten icat $HOME/Experiments/treated/logo/Final/final_white.svg
fi

# if [ "$TERM_PROGRAM" = "WezTerm" ]; then
#	flatpak run org.wezfurlong.wezterm imgcat $HOME/Experiments/treated/logo/Final/png/final_white_130dpi.png
# fi

# ============================================================================ #
# FZF

[ -f $HOME/.config/fzf/completion.bash ] && source $HOME/.config/fzf/completion.bash

# ============================================================================ #
# FOOT
alias "imls"="flatpak run org.wezfurlong.wezterm imgcat --width 80"

# ============================================================================ #
# WEZTERM
alias wezterm='flatpak run org.wezfurlong.wezterm'

# RICKROLL
alias "rickroll"="curl -s -L https://bit.ly/3zvELNz | bash"

# ROBOTICS PRINTER
alias roboprint="lpr -P HL1110"

# RASPBERRY PI
alias berry="sshpass -p "nixnix" ssh robotics@192.168.1.78"

# Spotify
alias 'music'="spicetify enable-devtools && exit"

# ROS
if [ "$(grep Ubuntu /etc/issue | awk '{print $1}')" == "Ubuntu" ]; then
	source /opt/ros/humble/setup.bash
fi

# IBUS
# ibus engine m17n:ne:rom-translit
# ibus engine xkb:us::eng
