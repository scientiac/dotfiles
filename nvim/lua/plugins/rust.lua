-- Configuration to enable rust tools in neovim

return {

  -- setting up mason for installing rust by default
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = { "rust-analyzer" },
    },
  },

  -- adding rust.vim plugin to auto format on save
  {
    "rust-lang/rust.vim",
    ft = "rust",
    init = function()
      vim.g.rustfmt_autosave = 1
    end,
  },

  -- using rust-tools for better rust lsp
  {
    "simrat39/rust-tools.nvim",
    ft = "rust",
    dependencies = "neovim/nvim-lspconfig",
    opts = function() end,
    config = function(_, opts)
      require("rust-tools").setup(opts)
    end,
  },
}
