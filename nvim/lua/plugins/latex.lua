-- configuration for LaTeX in neovim.
return {
  {
    "lervag/vimtex",
    opt = true,
    config = function()
      -- vim.g.vimtex_syntax_enabled = 0
      vim.g.vimtex_view_general_viewer = "zathura"
      vim.g.tex_comment_nospell = 1
      vim.g.vimtex_compiler_method = "latexmk"
      vim.g.tex_flavor = "latex"
      vim.g.vimtex_quickfix_mode = 0
      vim.opt.cc = "80"
    end,
    ft = "tex",
  },

  -- conceal
  {
    "KeitaNakamura/tex-conceal.vim",
    opt = true,
    config = function()
      vim.opt.conceallevel = 2
      vim.g.tex_conceal = "abdmg"
      vim.g.tex_conceal_frac = 1
    end,
    ft = "tex",
  },
}
